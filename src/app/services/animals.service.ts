import { Animal } from './../models/animal';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class AnimalsService {
  constructor(private _http: HttpClient) {}

  public getStatsFromRemote(): Observable<any> {
    return this._http.get<any>('http://localhost:5000/stats');
  }

  public createAnimalFromRemote(animal: Animal): Observable<any> {
    let token =
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsImlhdCI6MTYyMTI1MzU4Nn0.bZ-igXzd0feCTEJwnfr1jV_6buce4WLfE-n0EQeG4TY';
    const _httpHeaders = new HttpHeaders({
      'Content-type': 'application/json',
      Authorization: `Bearer ${token}`,
    });
    return this._http.post<any>(
      'http://localhost:5000/animals/create',
      animal,
      {
        headers: _httpHeaders,
      }
    );
  }

  getToken() {
    return JSON.stringify(localStorage.getItem('token'));
  }
}
