import { AuthGuard } from './auth.guard';
import { MyProfileComponent } from './components/myprofile/myprofile.component';
import { AnimalsService } from './services/animals.service';
import { RegistrationComponent } from './components/registration/registration.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationService } from './services/registration.service';
import { AddAnimalsComponent } from './components/myprofile/add-animals/add-animals.component';
import { AnimalsComponent } from './components/myprofile/animals/animals.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    MyProfileComponent,
    AddAnimalsComponent,
    AnimalsComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [RegistrationService, AnimalsService, AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
