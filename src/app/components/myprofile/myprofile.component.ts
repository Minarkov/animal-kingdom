import { AnimalsService } from '../../services/animals.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-login-sucess',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css'],
})
export class MyProfileComponent implements OnInit {
  constructor(private _service: AnimalsService) {}

  statsUsers = '';
  statsAnimals = '';

  ngOnInit(): void {}

  getStats() {
    this._service.getStatsFromRemote().subscribe((data) => {
      this.statsUsers = data.users;
      this.statsAnimals = data.animals;
    });
  }
}
