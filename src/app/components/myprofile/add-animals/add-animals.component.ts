import { Router } from '@angular/router';
import { AnimalsService } from './../../../services/animals.service';
import { Animal } from './../../../models/animal';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-animals',
  templateUrl: './add-animals.component.html',
  styleUrls: ['./add-animals.component.css'],
})
export class AddAnimalsComponent implements OnInit {
  animal = new Animal();
  constructor(private _service: AnimalsService, private _router: Router) {}

  ngOnInit(): void {}

  createAnimal() {
    this._service.createAnimalFromRemote(this.animal).subscribe((data) => {
      if (data.success == true) {
        this._router.navigate(['./animals']);
      } else {
        console.log(data);
      }
    });
  }
}
