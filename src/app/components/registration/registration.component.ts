import { RegistrationService } from './../../services/registration.service';
import { Router } from '@angular/router';
import { User } from '../../models/user';
import { NgForm } from '@angular/forms';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
  constructor(private _service: RegistrationService, private _router: Router) {}
  user = new User();
  msg = '';
  ngOnInit(): void {}

  registerUser() {
    this._service.registerUserFromRemote(this.user).subscribe((data) => {
      if (data.success == true) {
        this._router.navigate(['./login']);
        console.log(data);
      } else {
        this._router.navigate(['./registration']);
        this.msg = data.message;
      }
    });
  }

  goToLogIn() {
    this._router.navigate(['./login']);
  }
}
