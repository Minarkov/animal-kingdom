import { RegistrationService } from './../../services/registration.service';
import { User } from './../../models/user';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  user = new User();
  msg = '';
  constructor(private _service: RegistrationService, private _router: Router) {}

  ngOnInit(): void {}

  loginUser() {
    this._service.loginUserFromRemote(this.user).subscribe((data) => {
      if (data.success == true) {
        let token = data.token;
        this._router.navigate(['./myprofile']);
        localStorage.setItem('token', JSON.stringify(token));
      } else {
        this.msg = data.message;
      }
    });
  }

  goToRegistration() {
    this._router.navigate(['./registration']);
  }
}
